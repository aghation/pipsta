#!/usr/bin/python

'''This script demonstrate how to use the ESCPOS commands to change the text
 aspect.
'''
import platform
import sys
import time

import usb.core
import usb.util

FEED_PAST_CUTTER = b'\n' * 4
USB_BUSY = 66

PIPSTA_USB_VENDOR_ID = 0x0483
PIPSTA_USB_PRODUCT_ID = 0xA053

def send_text_to_printer(dev, usb_endpoint, txt):
    '''Loops through all the data and writes it out to the USB OUT enpoint,
    taking care of the Pipsta printers USB buffer'''
    for character in txt:
        usb_endpoint.write(character)

        res = dev.ctrl_transfer(0xC0, 0x0E, 0x020E, 0, 2)
        while res[0] == USB_BUSY:
            time.sleep(0.01)
            res = dev.ctrl_transfer(0xC0, 0x0E, 0x020E, 0, 2)

def main():
    if platform.system() != 'Linux':
        sys.exit('This script has only been written for Linux')

    dev = usb.core.find(idVendor=PIPSTA_USB_VENDOR_ID,
                        idProduct=PIPSTA_USB_PRODUCT_ID)
    if dev is None:  # if no such device is connected...
        raise IOError('Printer not found')  # ...report error

    try:
        # Linux requires USB devices to be reset before configuring, may not be
        # required on other operating systems.
        dev.reset()

        # Initialisation. Passing no arguments sets the configuration to the
        # currently active configuration.
        dev.set_configuration()
    except usb.core.USBError as ex:
        raise IOError('Failed to configure the printer', ex)

    # The following steps get an 'Endpoint instance'. It uses
    # PyUSB's versatile find_descriptor functionality to claim
    # the interface and get a handle to the endpoint
    cfg = dev.get_active_configuration()  # Get a handle to the active interface

    interface_number = cfg[(0, 0)].bInterfaceNumber
    # added to silence Linux complaint about unclaimed interface, it should be
    # release automatically
    usb.util.claim_interface(dev, interface_number)
    alternate_setting = usb.control.get_interface(dev, interface_number)
    interface = usb.util.find_descriptor(
        cfg, bInterfaceNumber=interface_number,
        bAlternateSetting=alternate_setting)

    usb_endpoint = usb.util.find_descriptor(
        interface,
        custom_match=lambda e:
        usb.util.endpoint_direction(e.bEndpointAddress) ==
        usb.util.ENDPOINT_OUT
    )

    if usb_endpoint is None:  # check we have a real endpoint handle
        raise IOError("Could not find an endpoint to print to")

	# Prints regular text.
    usb_endpoint.write(b'\x1b!\x00')
    txt = 'Regular\n'
    send_text_to_printer(dev, usb_endpoint, txt)

    # Prints text underlined.
    usb_endpoint.write(b'\x1b!\x80')
    txt = 'Underline\n'
    send_text_to_printer(dev, usb_endpoint, txt)

    # Prints text with a double width (16 chars per line, 57 mm)
    usb_endpoint.write(b'\x1b!\x20')
    txt = 'Double width\n'
    send_text_to_printer(dev, usb_endpoint, txt)

    # Prints text with a double height (32 chars per line, 57 mm)
    usb_endpoint.write(b'\x1b!\x10')
    txt = 'Double height'
    send_text_to_printer(dev, usb_endpoint, txt)

    usb_endpoint.write(FEED_PAST_CUTTER)
    usb.util.dispose_resources(dev)

# Ensure that BasicPrint is ran in a stand-alone fashion (as intended) and not
# imported as a module. Prevents accidental execution of code.
if __name__ == '__main__':
    main()
